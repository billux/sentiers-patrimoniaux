"use strict";

/**
 * Define Layers.
 */

var OSMLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
});
var THOutdoorsLayer = L.tileLayer('http://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=2a342dee09ea491d9bfbedb55c50ce8d', {
            attribution: '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
});

/**
 * Create map with THOutdoorsLayer as default layer.
 */

var map = L.map('map').setView([46.1, -74.25], 10);
THOutdoorsLayer.addTo(map);

/**
 * Add some Control objects to the map.
 */

var baseLayers = {
    "OpenStreetMap Outdoors Map": THOutdoorsLayer,
    "OpenStreetMap": OSMLayer,
};

var overlays = {};

L.control.layers(baseLayers, overlays).addTo(map);
L.control.scale({"imperial":false}).addTo(map);
//var sidebar = L.control.sidebar('sidebar', {position: 'right'}).addTo(map);

function createPisteFeature (object) {

    /*
     * Pistes are rendered in red if ungroomed (or grooming type is unknown for
     * them).
     * For groomed pistes, the color is set depending of piste:difficulty.
     */

    var color = 'red'; // default color
    var weight = 3;
    var dashStroke = '';
    if (object.hasOwnProperty('tags')) {
        if (object.tags['piste:grooming'] === 'backcountry' || !object.tags.hasOwnProperty('piste:grooming')) {
            color = 'red';
        }
        else {
            weight = 2;
            if (object.tags['piste:difficulty'] === 'easy') {
                color = 'green';
            }
            else if (object.tags['piste:difficulty'] === 'intermediate') {
                color = 'blue';
            }
            else {
                color = 'black';
            }
        }
        if (object.tags['trail_visibility'] === 'excellent') {
            dashStroke = '';
        }
        else if (object.tags['trail_visibility'] === 'good') {
            dashStroke = '10, 2';
        }
        else if (object.tags['trail_visibility'] === 'intermediate') {
            dashStroke = '6, 6';
        }
        else if (object.tags['trail_visibility'] === 'bad') {
            dashStroke = '4, 8';
        }
        else if (object.tags['trail_visibility'] === 'horrible') {
            dashStroke = '2, 10';
        }
        else if (object.tags['trail_visibility'] === 'no') {
            dashStroke = '1, 11';
        }
    }
    else {
        console.log('No tags found for object ' + object.id + ', ' + object.tags);
    }
    return object.leafletFeature({
        nodeFeature: 'CircleMarker',
        color: color,
        fillColor: color,
        fillOpacity: 0.1,
        weight: weight,
        radius: 6,
        dashArray: dashStroke
    });
}

/*
 * Query Overpass API with overpass-frontend lib to load piste:type=nordic ways
 * and relations from OpenStreetMap.
 */

const overpassFrontend = new OverpassFrontend('https://overpass-api.de/api/interpreter');
overpassFrontend.BBoxQuery(
    'way["piste:type"="nordic"]; relation["piste:type"="nordic"];',
    new BoundingBox(map.getBounds()),
    {
        properties: OverpassFrontend.ALL
    },
    function (error, object) {
        //console.log('Overpass query returns ' + object.tags.name + ' (' + object.id + ')');

        var id = object.id;
        var pistes = [];

        if (object.type === 'way') {

            /*
             * Create the Leaflet Feature for the piste with custom colour and
             * attach a popup with its OSM tags.
             */

            pistes[id] = {
                feature: createPisteFeature(object),
                tags: object.tags
            };
            pistes[id].feature.bindPopup('<h3>' + object.tags.name + '</h3> <p>Visibilité : ' + object.tags.trail_visibility + '</p><p>Données brutes OpenStreetMap :</p><pre>' + JSON.stringify(object.tags, null, '  ') + '</pre>', {maxWidth: 300, maxHeight: 450});
        }

        else if (object.type === 'relation') {

            object.memberFeatures.forEach(function (memberObject) {
                pistes[id] = {
                    feature: createPisteFeature(memberObject),
                    tags: memberObject.tags
                };
                if (!memberObject.hasOwnProperty('tags')) {
                    console.log('No tags found for object ' + memberObject.id + '(parent relation ' + object.id + '), ');
                    console.log(object);
                }
            });

            pistes[id].bindPopup('<pre>' + JSON.stringify(object.GeoJSON(), null, '  ') + '</pre>');

        }

        /*
        * Finally, add the piste to the map.
        */

        pistes[id].feature.addTo(map);

        /*
            * If the zoom level is high enough, add the name of the piste
            * along the polyline using textpath plugin.
            * We have to detect if the polyline is East-bound or West-bound
            * and flip the text accordingly though.
            */

        map.on('zoomend', function() {
            if (map.getZoom() < 14) {
                pistes[id].feature.setText(null);
            }
            else if (!pistes[id].feature._text) {
                var orientation = 0;
                if (pistes[id].feature.getLatLngs()[0].lng > pistes[id].feature.getLatLngs()[pistes[id].feature.getLatLngs().length - 1].lng) {
                    orientation = 'flip';
                }
                pistes[id].feature.setText(
                    pistes[id].tags.name,
                    {offset: -5, orientation: orientation}
                );
            }
        });
    },
    function (error) {
        if (error) {
            console.log(error);
        }
    }
);
